import { IImages } from "./Images.model";

export interface IResponseImage {
    total: number;
    totalHits: number;
    hits: IImages[];
}