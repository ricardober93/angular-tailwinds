import { Component, OnInit } from '@angular/core';
import { ImageService } from './servicies/image.service';
import { IImages } from './shared/models/Images.model';
import { IResponseImage } from './shared/models/Response.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public images: IImages[] = []
  constructor(private readonly imageService: ImageService) { }

  ngOnInit(): void {
    this.showImages()
  }

  showImages() {
    this.imageService.getIamges().subscribe((data: IResponseImage) => {
      this.images = data.hits
    });
  }

  public searchImage(word: string){
    if (word.length !== 0) {
      this.imageService.search(word).subscribe((data: IResponseImage) => {
        this.images = data.hits
      });
    } else {
      this.showImages()
    }
  }

  public FilterImage(category: string){
    if (category.length !== 0) {
      this.imageService.filter(category).subscribe((data: IResponseImage) => {
        this.images = data.hits
      });
    } else {
      this.showImages()
    }
  }
}
