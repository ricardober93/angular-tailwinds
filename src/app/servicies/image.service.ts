import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_BASE, API_TOKEN } from '../shared/constantes';
import { IImages } from '../shared/models/Images.model';
import { IResponseImage } from '../shared/models/Response.model';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  
  constructor(private http: HttpClient) { }

  public getIamges(){
    return this.http.get<IResponseImage>(`${API_BASE}?key=${API_TOKEN}`)
  }

  public search(word:string){
    return this.http.get<IResponseImage>(`${API_BASE}?key=${API_TOKEN}&q=${word}`)
  }

  public filter(category:string){
    return this.http.get<IResponseImage>(`${API_BASE}?key=${API_TOKEN}&category=${category}`)
  }
}
