export interface IImages {
    id: number;
    pageURL: string;
    type: string;
    collections: number;
    comments: number;
    downloads: number;
    imageHeight: number;
    imageSize: number;
    imageWidth: number;
    largeImageURL: string;
    likes: number;
    previewHeight: number;
    previewURL: string;
    previewWidth: number;
    tags: string;
    user: string;
    userImageURL: string;
    user_id: number;
    views: number;
    webformatHeight: number;
    webformatURL: string;
    webformatWidth: number;
}