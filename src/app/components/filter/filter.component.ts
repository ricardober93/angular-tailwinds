import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  filter: string | undefined = undefined ;

  @Output()
  filterEmitter = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }

  public filterSearch() {
    if (this.filter !== undefined) {
      this.filterEmitter.emit(this.filter);
    }
  }

}
