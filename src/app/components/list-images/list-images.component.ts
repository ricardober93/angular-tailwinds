import { Component, Input, OnInit } from '@angular/core';
import { IImages } from 'src/app/shared/models/Images.model';

@Component({
  selector: 'app-list-images',
  templateUrl: './list-images.component.html',
  styleUrls: ['./list-images.component.scss']
})
export class ListImagesComponent implements OnInit{
  @Input() images: IImages[] = []
  constructor() { }

  ngOnInit(): void {
    
    console.log(this.images);
    
  }


}
