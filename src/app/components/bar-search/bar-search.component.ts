import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-bar-search',
  templateUrl: './bar-search.component.html',
  styleUrls: ['./bar-search.component.scss']
})
export class BarSearchComponent implements OnInit {
  
  word: string = "";

  @Output()
  propagar = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }

  public search() {
    this.propagar.emit(this.word);
  }

}
